import React from "react";
import Enzyme, { shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import AboutPage from "./AppAbout";

Enzyme.configure({ adapter: new Adapter() });

describe("the about page ", () => {
    it("about page information", () => {
        const wrapper = shallow(<AboutPage />);
        const title = wrapper.find("div h1").text();
        expect(title).toBe("About Covid tracker");
    });

    it("about page paragraph information", () => {
        const wrapper = shallow(<AboutPage />);
        const title = wrapper.find("div p").text();
        expect(title).toBe(
            "This is the website for covid tracker. The application helps you to determine the covid stiuation."
        );
    });
});
