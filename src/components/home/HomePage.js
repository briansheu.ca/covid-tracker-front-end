import React from "react";
import Button from "react-bootstrap/Button";

export default function HomePage() {
    return (
        <div align="center">
            <h1>Welcome to Covide Tracker</h1>
            <div />
            <h6 align="left">What is Coronavirus?</h6>
            <iframe
                title="Covid"
                width="1019"
                height="586"
                src="https://www.youtube.com/embed/I-Yd-_XIWJg"
                frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
            ></iframe>
            <div />
            <a href="https://www.youtube.com/channel/UCPY9IKSkr2GfFW5jbgA5Qbw?feature=emb_ch_name_ex">
                By: Scientific Animations
            </a>
            <div />
            <a id="about" href="/about">
                <Button variant="outline-info">About</Button>
            </a>{" "}
            <a id="userlist" href="/userlist">
                <Button variant="outline-info">User List</Button>
            </a>
            <br />
            <br />
            <br />
            <br />
            <br />
        </div>
    );
}
