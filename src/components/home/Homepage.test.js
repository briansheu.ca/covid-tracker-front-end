import React from "react";
import Enzyme, { shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import HomePage from "./HomePage";

Enzyme.configure({ adapter: new Adapter() });

describe("HomePage ", () => {
    it("Welcome page", () => {
        const wrapper = shallow(<HomePage />);
        const title = wrapper.find("div h1").text();
        expect(title).toBe("Welcome to Covide Tracker");
    });

    it("contain 2 button", () => {
        const wrapper = shallow(<HomePage />);
        const anchorHref = wrapper.find("button");
        expect(anchorHref.length).toBe(2);
    });

    it("redirect to about page", () => {
        const wrapper = shallow(<HomePage />);
        const anchorHref = wrapper.find("#about").prop("href");
        expect(anchorHref).toEqual("/about");
    });

    it("redirect to about page", () => {
        const wrapper = shallow(<HomePage />);
        const anchorHref = wrapper.find("#userlist").prop("href");
        expect(anchorHref).toEqual("/userlist");
    });
});
