import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = () => {
    const activeStyle = { color: "black" };
    return (
        <nav>
            <NavLink to="/" activeStyle={activeStyle} exact>
                Home
            </NavLink>
            {" | "}
            <NavLink to="/about" activeStyle={activeStyle} exact>
                About
            </NavLink>
            {" | "}
            <NavLink to="/userlist" activeStyle={activeStyle} exact>
                Users
            </NavLink>
            {" | "}
            <NavLink to="/userlist/one" activeStyle={activeStyle} exact>
                Find User
            </NavLink>
            {" | "}
            <NavLink to="/userlist/deleteone" activeStyle={activeStyle} exact>
                Delete User
            </NavLink>
            {" | "}
            <NavLink to="/covid" activeStyle={activeStyle} exact>
                Covid-19
            </NavLink>
            {" | "}
            <NavLink to="/updatecovid" activeStyle={activeStyle} exact>
                update Covid-19 Information
            </NavLink>
            {" | "}
            <NavLink to="/login" activeStyle={activeStyle} exact>
                Login
            </NavLink>
            {" | "}
            <NavLink to="/logout" activeStyle={activeStyle} exact>
                Logout
            </NavLink>
        </nav>
    );
};

export default Navbar;
