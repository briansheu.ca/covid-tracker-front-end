import React from "react";
import { Link } from "react-router-dom";

function NotFoundPage() {
    return (
        <div align="center">
            <h1>
                ERROR: 404. Page not found (you asked for the wrong resource)
            </h1>
            <Link to="/">
                <button className="btn btn-warning">Back to home</button>
            </Link>
        </div>
    );
}

export default NotFoundPage;
