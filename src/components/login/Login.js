import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";

export default class login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            user_password: "",
        };

        this.change = this.change.bind(this);
        this.sumbit = this.sumbit.bind(this);
    }

    change(e) {
        this.setState({
            [e.target.name]: e.target.value,
        });
    }

    sumbit(e) {
        e.preventDefault();
        axios
            .post("http://18.222.54.113:5000/auth/login", {
                email: this.state.email,
                user_password: this.state.user_password,
            })
            .then(
                (res) => {
                    localStorage.setItem(
                        "jwt",
                        JSON.stringify(res.data.accessToken)
                    );
                    toast.success("Logined");
                }
                // .then((res) => {
                //     console.log(JSON.stringify(res.data));
            );
    }

    render() {
        return (
            <div className="form-group">
                <form onSubmit={(e) => this.sumbit(e)}>
                    <label>Email</label>{" "}
                    <input
                        type="text"
                        name="email"
                        className="form-control"
                        onChange={(e) => this.change(e)}
                        value={this.state.email}
                    />
                    <label>Password</label>
                    <input
                        type="text"
                        name="user_password"
                        className="form-control"
                        onChange={(e) => this.change(e)}
                        value={this.state.user_password}
                    />
                    <br></br>
                    <button type="submit" className="btn btn-primary">
                        Submit
                    </button>
                    {" | "}
                    <Link to="/login/create">
                        <button className="btn btn-primary">Register</button>
                    </Link>
                </form>
            </div>
        );
    }
}
