import React from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";

export default class Logout extends React.Component {
    logout = toast.success("You Are logged Out");
    render() {
        return (
            <div className="form-group">
                <form onSubmit={localStorage.clear()}>
                    <Link to="/login">
                        <button className="btn btn-info">
                            Logout! Click to Return to Login
                        </button>
                    </Link>
                </form>
            </div>
        );
    }
}
