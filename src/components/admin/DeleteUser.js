import React from "react";
import { toast } from "react-toastify";
// import axios from "axios";
import { getJwt } from "../helpers/jwt";
import Button from "react-bootstrap/Button";

export default class DeleteUser extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            full_name: "",
            user_password: "",
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSumbit = this.handleSumbit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    handleSumbit(event) {
        event.preventDefault();
        let jwt = getJwt();
        console.log(jwt.length);
        let token = jwt.slice(1, jwt.length - 1);

        const myHeader = new Headers();
        myHeader.append("Content-Type", "application/json");
        myHeader.append("Authorization", `Bearer ${token}`);

        fetch("http://18.222.54.113:5000/admin/deleteOneUser", {
            method: "DELETE",
            headers: myHeader,
            body: JSON.stringify({
                full_name: this.state.full_name,
                user_password: this.state.user_password,
            }),
        }).then((result) => {
            toast.success("deleted");
            this.props.history.push("/userlist");
        });
    }

    render() {
        return (
            <div>
                <br />
                <h3> Delete a Register User</h3>
                <br />
                <form onSubmit={(event) => this.handleSumbit(event)}>
                    <input
                        type="text"
                        name="full_name"
                        className="form-control"
                        placeholder="Enter User's Full Name"
                        onChange={(event) => {
                            this.handleChange(event);
                        }}
                        value={this.state.full_name}
                    />
                    <br />
                    <input
                        type="text"
                        name="user_password"
                        className="form-control"
                        placeholder="Enter User's Password"
                        onChange={(event) => this.handleChange(event)}
                        value={this.state.user_password}
                    />
                    <br />
                    <Button variant="danger" type="submit">
                        Sumbit
                    </Button>
                </form>
            </div>
        );
    }
}
