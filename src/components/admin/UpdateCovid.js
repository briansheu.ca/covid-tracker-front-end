import React from "react";
import { getJwt } from "../helpers/jwt";
// import axios from "axios";
import { toast } from "react-toastify";

export default class UpdateCovid extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            covid_state_name: "",
            confirmed_cases: "",
            deaths: "",
            recoveries: "",
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSumbit = this.handleSumbit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    handleSumbit(event) {
        event.preventDefault();
        let jwt = getJwt();
        console.log(jwt.length);
        let token = jwt.slice(1, jwt.length - 1);
        console.log(token);
        const myHeader = new Headers();
        myHeader.append("Content-Type", "application/json");
        myHeader.append("Authorization", `Bearer ${token}`);

        fetch("http://18.222.54.113:5000/admin/updateOneCovid", {
            method: "PUT",
            headers: myHeader,
            body: JSON.stringify({
                covid_state_name: this.state.covid_state_name,
                confirmed_cases: this.state.confirmed_cases,
                deaths: this.state.deaths,
                recoveries: this.state.recoveries,
            }),
        }).then((result) => {
            toast.success("Updated");
        });
    }

    //     axios
    //         .put("http://localhost:5000/admin/updateOneCovid", {
    //             // headers: { Authorization: "Bearer" + token },
    //             headers: { Authorization: `Bearer ${jwt}` },
    //             body: JSON.stringify({
    // covid_state_name: this.state.covid_state_name,
    // confirmed_cases: this.state.confirmed_cases,
    // deaths: this.state.deaths,
    // recoveries: this.state.recoveries,
    //             }),
    //         })

    //         .then((result) => {
    //             toast.success("updated");
    //         })
    //         .catch((err) => console.log(err));
    // }

    render() {
        return (
            <div>
                <form onSubmit={(event) => this.handleSumbit(event)}>
                    <label>Update Covid-19 Info: </label>{" "}
                    <input
                        type="text"
                        name="covid_state_name"
                        className="form-control"
                        placeholder="state"
                        onChange={(event) => {
                            this.handleChange(event);
                        }}
                        value={this.state.covid_state_name}
                    />
                    <br />
                    <input
                        type="text"
                        name="confirmed_cases"
                        className="form-control"
                        placeholder="Confirmed Cases"
                        onChange={(event) => {
                            this.handleChange(event);
                        }}
                        value={this.state.confirmed_cases}
                    />
                    <br />
                    <input
                        type="text"
                        name="deaths"
                        className="form-control"
                        placeholder="Deaths"
                        onChange={(event) => {
                            this.handleChange(event);
                        }}
                        value={this.state.deaths}
                    />
                    <br />
                    <input
                        type="text"
                        name="recoveries"
                        className="form-control"
                        placeholder="Recoveries"
                        onChange={(event) => {
                            this.handleChange(event);
                        }}
                        value={this.state.recoveries}
                    />
                    <br />
                    <div>
                        <button type="submit" className="btn btn-info">
                            Sumbit
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}
