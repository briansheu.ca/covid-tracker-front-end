import React from "react";
import axios from "axios";
import Button from "react-bootstrap/Button";

export default class GetOneCovid extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            covid: [],
            // full_name: "",
            // user_role: "",
            covid_state_name: "",
            // health_status: "",
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSumbit = this.handleSumbit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    handleSumbit(event) {
        event.preventDefault();
        axios
            .post("http://18.222.54.113:5000/covid/getOne", {
                covid_state_name: this.state.covid_state_name,
            })
            // .then((result) => console.log(result))  for some reason, you cant not console. log first
            .then((result) => {
                this.setState({ covid: result.data });
            })
            .catch((err) => console.log(err));
    }

    render() {
        return (
            <div>
                <form onSubmit={(event) => this.handleSumbit(event)}>
                    <label>Find Covid Condition by: </label>{" "}
                    <input
                        type="text"
                        name="covid_state_name"
                        className="form-control"
                        placeholder="Enter Full State"
                        onChange={(event) => {
                            this.handleChange(event);
                        }}
                        value={this.state.covid_state_name}
                    />
                    <br />
                    <Button variant="warning" type="submit">
                        Sumbit
                    </Button>
                </form>
                <br />
                <table className="table">
                    <thead>
                        <tr>
                            <th>State</th>
                            <th>Confirmed Cases</th>
                            <th>Deaths</th>
                            <th>Recoveries</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.covid.map((covid) => {
                            return (
                                <tr key={covid.covid_state_name}>
                                    <td>{covid.covid_state_name} </td>
                                    <td>{covid.confirmed_cases} </td>
                                    <td>{covid.deaths} </td>
                                    <td>{covid.recoveries} </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}
