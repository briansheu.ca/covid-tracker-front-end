import React from "react";
import Enzyme, { shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import UserList from "./UserList";

Enzyme.configure({ adapter: new Adapter() });

describe("Userlist component ", () => {
    it("should give list", () => {
        const wrapper = shallow(<UserList />);
        const table = wrapper.find("div table.table");
        expect(table.length).toBe(1);
    });

    it("populate table", () => {
        const wrapper = shallow(<UserList />);
        const rows = wrapper.find("tr");
        expect(rows.length).toBe(1);
    });
});
