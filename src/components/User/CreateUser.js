import React, { useState } from "react";
import CreateUserForm from "../User/CreatUserForm";
import { toast } from "react-toastify";

export default function CreateUser(props) {
    const [user, setUser] = useState({
        full_name: "",
        user_role: "Guest",
        user_password: "",
        user_state: "",
        health_status: "",
        address: "",
        email: "",
        id: "",
        phone_number: "",
    });

    function handleChange(event) {
        setUser({ ...user, [event.target.name]: event.target.value });
    }

    function handleSumbit(event) {
        event.preventDefault();
        fetch("http://18.222.54.113:5000/users/new", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(user),
        })
            .then((result) => {
                toast.success("Registered");
                props.history.push("/userlist");
            })
            .catch((err) => {
                toast.error("Something went wrong...");
            });
    }

    return (
        <div>
            <h3>Register Below</h3>
            <CreateUserForm
                onSubmit={handleSumbit}
                onChange={handleChange}
                user={user}
            />
        </div>
    );
}
