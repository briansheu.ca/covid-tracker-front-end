import React from "react";

export default class UserList extends React.Component {
    state = {
        users: [],
    };

    componentDidMount() {
        fetch("http://18.222.54.113:5000/users/getall")
            .then((response) => response.json())
            .then((data) => {
                this.setState({
                    users: data,
                });
            })
            .catch((err) => console.log(err));
    }

    render() {
        return (
            <div>
                <h3>User List Page</h3>
                <table className="table" variant="dark">
                    <thead>
                        <tr>
                            <th>Full Name</th>
                            <th>User Role</th>
                            <th>Password</th>
                            <th>State</th>
                            <th>Health Status</th>
                            <th>Adress</th>
                            <th>Email</th>
                            <th>ID</th>
                            <th>Phone Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.users.map((user) => {
                            return (
                                <tr key={user.id}>
                                    <td>{user.full_name}</td>
                                    <td>{user.user_role}</td>
                                    <td>{"Unavailable"}</td>
                                    <td>{user.user_state}</td>
                                    <td>{user.health_status}</td>
                                    <td>{user.address}</td>
                                    <td>{user.email}</td>
                                    <td>{user.id}</td>
                                    <td>{user.phone_number}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}
