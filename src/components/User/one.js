import React from "react";
import axios from "axios";
import Button from "react-bootstrap/Button";

export default class OneList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: [],
            // full_name: "",
            // user_role: "",
            user_state: "",
            // health_status: "",
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSumbit = this.handleSumbit.bind(this);
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    handleSumbit(event) {
        event.preventDefault();
        axios
            .post("http://18.222.54.113:5000/users/getByState", {
                user_state: this.state.user_state,
            })
            // .then((result) => console.log(result))
            .then((result) => {
                this.setState({ user: result.data });
            })
            .catch((err) => console.log(err));
    }

    render() {
        return (
            <div>
                <form onSubmit={(event) => this.handleSumbit(event)}>
                    <label>Find User by the State</label>{" "}
                    <input
                        type="text"
                        name="user_state"
                        className="form-control"
                        placeholder="Enter Full State"
                        onChange={(event) => {
                            this.handleChange(event);
                        }}
                        value={this.state.user_state}
                    />
                    <br />
                    <Button variant="dark" type="submit">
                        Sumbit
                    </Button>
                </form>
                <br />
                <table className="table">
                    <thead>
                        <tr>
                            <th>Full Name</th>
                            <th>User Role</th>
                            <th>User's State</th>
                            <th>Health Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.user.map((user) => {
                            return (
                                <tr key={user.full_name}>
                                    <td>{user.full_name} </td>
                                    <td>{user.user_role} </td>
                                    <td>{user.user_state} </td>
                                    <td>{user.health_status} </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}
