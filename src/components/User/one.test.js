import React from "react";
import Enzyme, { shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import OneList from "./one";

Enzyme.configure({ adapter: new Adapter() });

describe("One list component ", () => {
    it("should give form", () => {
        const wrapper = shallow(<OneList />);
        const form = wrapper.find("form");
        expect(form.length).toBe(1);
    });

    it("populate table", () => {
        const wrapper = shallow(<OneList />);
        const rows = wrapper.find("tr");
        expect(rows.length).toBe(1);
    });
});
