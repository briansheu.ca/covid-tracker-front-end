import React from "react";
import TextInput from "../Commom/TextInput";

export default function CreateUserForm(props) {
    return (
        <form onSubmit={props.onSubmit}>
            <TextInput
                id="full_name"
                label="Full Name"
                onChange={props.onChange}
                onSubmit={props.onSubmit}
                name="full_name"
                value={props.user.full_name}
            />
            {/* <TextInput
                id="user_role"
                label="user_role"
                user
                onChange={props.onChange}
                onSubmit={props.onSubmit}
                name="user_role"
                value={props.user.user_role}
            /> */}
            <TextInput
                id="user_password"
                label="User Password"
                onChange={props.onChange}
                onSubmit={props.onSubmit}
                name="user_password"
                value={props.user.user_password}
            />
            <TextInput
                id="user_state"
                label="State"
                onChange={props.onChange}
                onSubmit={props.onSubmit}
                name="user_state"
                value={props.user.user_state}
            />
            <TextInput
                id="health_status"
                label="Health Status"
                onChange={props.onChange}
                onSubmit={props.onSubmit}
                name="health_status"
                value={props.user.health_status}
            />
            <TextInput
                id="address"
                label="Address"
                onChange={props.onChange}
                onSubmit={props.onSubmit}
                name="address"
                value={props.user.address}
            />
            <TextInput
                id="email"
                label="Email"
                onChange={props.onChange}
                onSubmit={props.onSubmit}
                name="email"
                value={props.user.email}
            />
            <TextInput
                id="phone_number"
                label="Phone Number"
                onChange={props.onChange}
                onSubmit={props.onSubmit}
                name="phone_number"
                value={props.user.phone_number}
            />

            <button type="submit" className="btn btn-primary">
                Submit
            </button>
            <br />
            <br />
            <br />
            <br />
        </form>
    );
}
