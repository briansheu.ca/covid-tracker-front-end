import React from "react";
import HomePage from "../src/components/home/HomePage";
import AboutPage from "./components/about/AppAbout";
import "./App.css";
import UserList from "./components/User/UserList";
// import GetOneUser from "./components/UserList/GetOneUser";
// import Authenticate from './components/authenticate/Authenticate';
import Login from "./components/login/Login";
import Navbar from "./components/Commom/NavBar";
import { Switch, Route } from "react-router-dom";
import NotFoundPage from "./components/Commom/NotFoundPage";
import CreateUser from "./components/User/CreateUser";
// import GetOneUser from "./components/User/GetOneUser";
import { ToastContainer } from "react-toastify";
import one from "./components/User/one";
import GetOneCovid from "./components/covid/GetOneCovid";
import DeleteUser from "./components/admin/DeleteUser";
import UpdateCovid from "./components/admin/UpdateCovid";
import Logout from "./components/login/Logout";

function App() {
    return (
        <div className="container" style={{ backgroundColor: "white" }}>
            <Navbar />
            <br />
            <br />
            <ToastContainer autoClose={3000} hideProgressBar />
            <Switch>
                <Route path="/about" component={AboutPage} exact />
                <Route path="/userlist" component={UserList} exact />
                <Route path="/login" component={Login} exact />
                <Route path="/login/create" component={CreateUser} exact />
                <Route path="/" component={HomePage} exact />
                <Route path="/userlist/one" component={one} exact />
                <Route
                    path="/userlist/deleteone"
                    component={DeleteUser}
                    exact
                />
                <Route path="/covid" component={GetOneCovid} exact />
                <Route path="/updatecovid" component={UpdateCovid} exact />
                <Route path="/logout" component={Logout} exact />
                <Route component={NotFoundPage} />
            </Switch>
        </div>
    );
}

export default App;
